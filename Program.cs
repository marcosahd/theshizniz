﻿using System;
using System.Threading.Tasks;
using WooCommerceNET;
using WooCommerceNET.WooCommerce.v3;
using System.Linq;
using LamaNet___Integration_Test.Class;
using TheApp.Settings;
using System.Threading;

namespace LamaNet___Integration_Test
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            Settings settings = new Settings();
            Sql client = new Sql(settings);
            while (true)
            {
                try
                {   
                    RestAPI rest = new RestAPI($"{settings.SiteUrl}/wp-json/wc/v3/", settings.SiteKey, settings.SiteSecret);
                    WCObject wc = new WCObject(rest);

                    //Get all orders
                    var orders = await wc.Order.GetAll();


                    orders.ForEach(order =>
                    {
                        if (order.status == "processing")
                        {
                            Console.WriteLine("Order status = processing");

                        //Call Insert function to insert these orders
                        client.Process(order);
                        }
                        else
                        {
                            Console.WriteLine(order.status);
                            Console.WriteLine(order.date_created);
                        }


                    });
                }
                catch (Exception x)
                {
                    Console.WriteLine(x.Message);
                }

                Thread.Sleep(TimeSpan.FromMinutes(1));
            }

            Console.ReadLine();

        }
    }
}