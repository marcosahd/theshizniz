﻿using System.Data.SqlClient;
using DbContext = System.Data.Entity.DbContext;
using System;
using TheApp.Settings;
using WooCommerceNET.WooCommerce.v3;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;

namespace LamaNet___Integration_Test.Class
{
    public class Sql : DbContext
    {
        private readonly Settings _settings;

        public Sql(Settings settings)
        {
            _settings = settings;
            CreateOrders();
            CreateShipping();
            CreateBilling();
            CreateLineitems();
        }

        public void Process(Order order)
        {
            if (!Check(order.id.ToString()))
            {
                InsertOrder(order);
                InsertBilling(order);
                InsertLineItems(order);
                InsertShipping(order);
            }
            else
            {
                Console.WriteLine("Order Exists - Skipping");
            }
        }


        private void Insert(string Command)
        {
            try
            {
                using (var connection = new SqlConnection(_settings.SQLConnectionString))
                {
                    connection.Open();

                    //Insert WCommerce Records
                    using (var transaction = connection.BeginTransaction())
                    {
                        var insertCommand = connection.CreateCommand();
                        insertCommand.CommandText = Command;
                        insertCommand.Transaction = transaction;
                        insertCommand.ExecuteNonQuery();
                        transaction.Commit();
                    }
                    connection.Close();
                }
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        private bool Check(string Id)
        {
            using (SqlConnection connection = new SqlConnection(_settings.SQLConnectionString))
            using (SqlCommand command = new SqlCommand($"select * from Orders where id = {Id}", connection))
            {
                connection.Open();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                connection.Close();
            }
        }

        private void CreateOrders()
        {
            try
            {
                using (var connection = new SqlConnection(_settings.SQLConnectionString))
                {
                    connection.Open();

                    // Create table 
                    var tableCommand = connection.CreateCommand();
                    tableCommand.CommandText = "CREATE TABLE Orders (" +
                                                "id int NOT NULL PRIMARY KEY," +
                                                "parent_id int NOT NULL," +
                                                "number varchar(150) NOT NULL," +
                                                "order_key varchar(150) NOT NULL," +
                                                "created_via varchar(150) NOT NULL," +
                                                "version varchar(150) NOT NULL," +
                                                "status varchar(150) NOT NULL," +
                                                "currency varchar(150) NOT NULL," +
                                                "date_created DateTime NOT NULL," +
                                                "date_modified DateTime NOT NULL," +
                                                "shipping_total varchar(150) NOT NULL," +
                                                "cart_tax varchar(150) NOT NULL," +
                                                "total varchar(150) NOT NULL," +
                                                "total_tax varchar(150) NOT NULL," +
                                                "customer_id int NOT NULL," +
                                                "customer_ip_address varchar(150) NOT NULL," +
                                                "payment_method varchar(150) NOT NULL," +
                                                "transaction_id varchar(150)," +
                                            ");";

                    tableCommand.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }

        }

        private void CreateLineitems()
        {
            try
            {
                using (var connection = new SqlConnection(_settings.SQLConnectionString))
                {
                    connection.Open();

                    // Create table 
                    var tableCommand = connection.CreateCommand();
                    tableCommand.CommandText = "CREATE TABLE LineItems (" +
                                                "orderid int FOREIGN KEY REFERENCES Orders(id)," +
                                                "id int NOT NULL," +
                                                "name varchar(150) NOT NULL," +
                                                "product_id int NOT NULL," +
                                                "variation_id int NOT NULL," +
                                                "quantity int NOT NULL," +
                                                "subtotal varchar(150) NOT NULL," +
                                                "total varchar(150) NOT NULL," +
                                                "total_tax varchar(150) NOT NULL," +
                                            ");";

                    tableCommand.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }

        }

        private void CreateBilling()
        {
            try
            {
                using (var connection = new SqlConnection(_settings.SQLConnectionString))
                {
                    connection.Open();

                    // Create table 
                    var tableCommand = connection.CreateCommand();
                    tableCommand.CommandText = "CREATE TABLE Billing (" +
                                                "orderid int FOREIGN KEY REFERENCES Orders(id)," +
                                                "first_name varchar(150)," +
                                                "last_name varchar(150)," +
                                                "company varchar(150)," +
                                                "address_1 varchar(150)," +
                                                "address_2 varchar(150)," +
                                                "city varchar(150)," +
                                                "state varchar(150)," +
                                                "postcode varchar(150), " +
                                                "country varchar(150)," +
                                                "email varchar(150)," +
                                                "phone varchar(150)," +
                                            ");";

                    tableCommand.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }

        }

        private void CreateShipping()
        {
            try
            {
                using (var connection = new SqlConnection(_settings.SQLConnectionString))
                {
                    connection.Open();

                    // Create table 
                    var tableCommand = connection.CreateCommand();
                    tableCommand.CommandText = "CREATE TABLE Shipping (" +
                                                "orderid int FOREIGN KEY REFERENCES Orders(id)," +
                                                "first_name varchar(150)," +
                                                "last_name varchar(150)," +
                                                "company varchar(150)," +
                                                "address_1 varchar(150)," +
                                                "address_2 varchar(150)," +
                                                "city varchar(150)," +
                                                "state varchar(150)," +
                                                "postcode varchar(150)," +
                                                "country varchar(150)," +
                                            ");";

                    tableCommand.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }

        }

        private void InsertBilling(Order order)
        {
            try
            {
                string Command = $"INSERT INTO Billing(orderid, first_name, last_name, " +
                                                    $"company, address_1, address_2, " +
                                                    $"city, state, postcode, " +
                                                    $"country, email, phone )" +
                                                    $"VALUES ({order.id}, '{order.billing.first_name}', '{order.billing.last_name}', " +
                                                    $"'{order.billing.company}', '{order.billing.address_1}', '{order.billing.address_2}', " +
                                                    $"'{order.billing.city}', '{order.billing.state}', '{order.billing.postcode}', " +
                                                    $"'{order.billing.country}', '{order.billing.email}', '{order.billing.phone}')";
                Insert(Command);
                Console.WriteLine($"Inserted Billing for {order.id}");
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }
        }

        private void InsertOrder(Order order)
        {
            try
            {
                string Command = $"INSERT INTO Orders(id, parent_id, number, " +
                                                        $"order_key, created_via, version, " +
                                                        $"status, currency, date_created, " +
                                                        $"date_modified, shipping_total, cart_tax, " +
                                                        $"total, total_tax, customer_id, " +
                                                        $"customer_ip_address, payment_method, transaction_id )" +
                                                        $" VALUES ({order.id}, {order.parent_id}, '{order.number}', " +
                                                        $"'{order.order_key}', '{order.created_via}', '{order.version}', " +
                                                        $"'{order.status}', '{order.currency}', '{order.date_created.ToString()}', " +
                                                        $"'{order.date_modified.ToString()}', '{order.shipping_total}', '{order.cart_tax}'," +
                                                        $"'{order.total}', '{order.total_tax}', {order.customer_id}," +
                                                        $"'{order.customer_ip_address}', '{order.payment_method}', '{order.transaction_id}')";
                Insert(Command);
                Console.WriteLine($"Inserted Order for {order.id}");
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }

        }

        private void InsertShipping(Order order)
        {
            try
            {
                string Command = $"INSERT INTO Shipping(orderid, first_name, last_name, " +
                                                    $"company, address_1, address_2, " +
                                                    $"city, state, postcode, " +
                                                    $"country)" +
                                                    $" VALUES ({order.id}, '{order.shipping.first_name}', '{order.shipping.last_name}', " +
                                                    $"'{order.shipping.company}', '{order.shipping.address_1}', '{order.shipping.address_2}', " +
                                                    $"'{order.shipping.city}', '{order.shipping.state}', '{order.shipping.postcode}', " +
                                                    $"'{order.shipping.country}')";
                Insert(Command);
                Console.WriteLine($"Inserted Shipping for {order.id}");
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }
        }

        private void InsertLineItems(Order order)
        {
            order.line_items?.ForEach(item =>
            {
                try
                {
                    string Command = $"INSERT INTO LineItems(orderid, id, name, product_id, " +
                                                    $"variation_id, quantity, subtotal, " +
                                                    $"total, total_tax)" +
                                                    $"VALUES ({order.id}, {item.id}, '{item.name}', {item.product_id}, " +
                                                    $"{item.variation_id}, {item.quantity}, '{item.subtotal}', " +
                                                    $"'{item.total}', {item.total_tax})";

                    Insert(Command);
                    Console.WriteLine($"Inserted Items for {order.id}");
                }
                catch (Exception x)
                {
                    Console.WriteLine(x.Message);
                }
            });
        }
    }
}
